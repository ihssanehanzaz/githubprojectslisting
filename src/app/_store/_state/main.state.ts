export interface Project {
  id: number;
}


export interface MainState {
  projects: Array<Project>;
}

export const initialState: MainState = {
  projects: []
};
