import { Action } from '@ngrx/store';
// import { SignInRequestPayload } from './libelles.effects.payloads';

export const EffectTypes = {
    GET_PROJECTS_EFFECT: '[projectEffect] GET_PROJECTS_EFFECT',
};

export class GetProjectsEffect implements Action {
    type = EffectTypes.GET_PROJECTS_EFFECT;
    constructor(
      public payload?: {
        page?: number;
      }) {}
}

export type All = GetProjectsEffect;
