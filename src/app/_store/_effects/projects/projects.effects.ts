import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import { HttpClient } from '@angular/common/http';
import * as ProjectsActions from '../../_actions/projects.actions';
import * as ProjectsEffectsTypes from './projects.effects.types';
import { AppEffect } from '../app.effect';
// import { SignInRequestPayload } from './auth.effects.payloads';

export type Action = ProjectsEffectsTypes.All;

@Injectable()
export class ProjectsEffects extends AppEffect {
  constructor(
    private action$: Actions<Action>,
    private http: HttpClient
  ) {
    super(http);
  }

  @Effect()
   projects$: Observable<
    ProjectsActions.GetProjects | ProjectsActions.GetProjectsError
    > = this.action$.pipe(ofType(ProjectsEffectsTypes.EffectTypes.GET_PROJECTS_EFFECT))
      .switchMap(action =>
        // get data from Rest API
        this.get$(this.apiUrl + `search/repositories?q=created:>2017-10-22&sort=stars&order=desc&page=${action.payload.page}`)
          .map((data: any) => {
            // return the action that will be maped to Observable<Action>
            return new ProjectsActions.GetProjects({
             projects : data.items
            });
          })
          .catch(errors => {
            console.error(errors);
            return of(new ProjectsActions.GetProjectsError(errors));
          })
      )
      .share();

}
