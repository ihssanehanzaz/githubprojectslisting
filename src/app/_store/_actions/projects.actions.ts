import { Action } from '@ngrx/store';
import {Project} from '../_state/main.state';

export const ActionTypes = {
  GET_PROJECTS: '[Projects] GET_PROJECTS',
  GET_PROJECTS_ERROR: '[Projects] GET_PROJECTS_ERROR'
};

export class GetProjects implements Action {
  type = ActionTypes.GET_PROJECTS;

  constructor(
    public payload: {
      projects: Array<Project>;
    }
  ) { }
}

export class GetProjectsError implements Action {
  type = ActionTypes.GET_PROJECTS_ERROR;

  constructor(public payload: { err: any }) { }
}


export type All =
  | GetProjects
  | GetProjectsError;
