import { ActionReducerMap } from '@ngrx/store';
import { MainState } from '../_state/main.state';
import { ProjectsReducer } from './projects/projects.reducer';

export const reducers: ActionReducerMap<MainState> = {
  projects : ProjectsReducer
};
