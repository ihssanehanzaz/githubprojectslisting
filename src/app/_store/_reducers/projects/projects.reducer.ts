import * as projectsactions from '../../_actions/projects.actions';
import { initialState, Project} from '../../_state/main.state';
import createReducer from '../../createReducer';

export type Action = projectsactions.All;

export function ProjectsReducer(
  incomingState: Array<Project> = initialState.projects,
  incomingAction: Action
): Array<Project> {
  return createReducer({
    [projectsactions.ActionTypes.GET_PROJECTS]: (
      state: Array<Project>,
      action: projectsactions.GetProjects
    ) => {
      return action.payload.projects;
    }
  })(incomingState, incomingAction);
}
