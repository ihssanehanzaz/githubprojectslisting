import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RouterModule } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import {AppRoutes} from './app.routing';
import { reducers } from './_store/_reducers';
import { effects } from './_store/_effects';
import {initialState} from './_store/_state/main.state';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers, {initialState}),
    EffectsModule.forRoot(effects),
    RouterModule.forRoot(AppRoutes),
   // StoreModule.forRoot(reducers  initialState });
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

