import {Component, OnDestroy, OnInit, ChangeDetectorRef, OnChanges} from '@angular/core';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {MainState} from './_store/_state/main.state';
import {GetProjectsEffect} from './_store/_effects/projects/projects.effects.types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit, OnDestroy {
  private subscription: Subscription;
  public page = 2;
  public maxCollectionSize = 1000;
  projects = [];
  constructor(private store: Store<MainState>) {
    // subscription
    this.subscription = new Subscription();
    // sub store Projects
    const sublistprojects = this.store.select('projects').subscribe(projects => {
      if (projects) {
        this.projects = projects;
      }
    });
    this.subscription.add(sublistprojects);
  }
  ngOnInit() {
  }
  getAllProjects(setPage = 1) {
    const payload = {page : setPage};
    this.store.dispatch(new GetProjectsEffect(payload));
  }
  // destroy Sub
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
