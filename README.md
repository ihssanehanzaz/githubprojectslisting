# GitHub Listing project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Features

* As a User I should be able to list the most starred Github repos that were created in the last 30 days.
* As a User I should see the results as a list. One repository per row.
* As a User I should be able to see for each repo/row the following details :
   Repository name
   Repository description
   Number of stars for the repo.
   Number of issues for the repo.
   Username and avatar of the owner.
 * As a User I should be able to move from page to another page (pagination).
 
 ## Technologies 
 
### Angular v7
Angular helps you build modern applications for the web, mobile, or desktop.

### NgRx 
gRx Store provides reactive state management for Angular apps inspired by Redux. Unify the events in your application and derive state using RxJS.
i know i was able to use just simple service for get data and show all items, but i worked with ngRx just to let you know that im using Store lib such as also ngrx, mobx [...] for managing the store. 

### ng Bootstrap
Bootstrap widgets : 
Angular widgets built from the ground up using only Bootstrap 4 CSS with APIs designed for the Angular ecosystem.
 i used ngb-pagination for paginate listing.
